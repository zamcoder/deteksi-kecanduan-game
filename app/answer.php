<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class answer extends Model
{
    protected $fillable = [
        'quest_id', 'answer'
    ];
    protected $table = 'answer';
    public function quest()
    {
        return $this->belongsTo('App\quest');
    }
}
