<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quest extends Model
{
    protected $fillable = [
        'question'
    ];
    protected $table = 'quest';
    public function answer()
    {
        return $this->hasMany('App\answer');
    }
}
