<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_peserta extends Model
{
    protected $fillable = [
        'email', 'nama', 'umur', 'jenis_kelamin'
    ];
    protected $table = 'data_peserta';
}
