<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class point extends Model
{
    protected $fillable = [
        'answer_id', 'point_percent'
    ];
    protected $table = 'point';
}
