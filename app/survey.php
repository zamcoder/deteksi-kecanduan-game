<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class survey extends Model
{
    protected $fillable = [
        'data_peserta_id', 'quest_id', 'answer_id', 'point_id'
    ];
    protected $table = 'survey';
}
