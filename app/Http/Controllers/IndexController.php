<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\User;
use App\answer;
use App\data_peserta;
use App\point;
use App\quest;
use App\survey;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use File;

class IndexController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
    public function post(Request $request)
    {
        $data = data_peserta::where('email', $request->get('email'))->get();
        $data_count = $data->count();
        if ($data_count > 0) {
            foreach($data as $d) {
                return response()->json([
                    'success' => true,
                    'id' => $d->id
                ], 201);
            }
        }
        else {
            $data_peserta = new data_peserta([
                'email' => $request->get('email'),
                'nama' => $request->get('nama'),
                'umur' => $request->get('umur'),
                'jenis_kelamin' => $request->get('jenis_kelamin'),
            ]);
            $data_peserta->save();
            return response()->json([
                'success' => true,
                'id' => $data_peserta->id
            ], 201);
        }
    }
    public function test($id)
    {
        $validation = survey::where('data_peserta_id', $id)->get();
        $val_count = $validation->count();

        if($val_count > 0) {
            return redirect()->route('result', ['id' => $id]);
        }
        else{
            $data = data_peserta::where('id', $id)->get();
            $data_qa = quest::with('answer')->get();
            $data_count = $data_qa->count();
            return view('test', ['data' => $data, 'data_count' => $data_count], compact('data_qa'));
        }
    }
    public function test_post(Request $request)
    {
        $data_count = (int) $request->get('total_question');

        for($i=1; $i <= $data_count; $i++){

            $question = $request->get('question_'.$i);
            $answer = $request->get('answer_'.$i);

            $data = DB::table('answer')
                        ->join('point', 'answer.id', '=', 'point.answer_id')
                        ->select('answer.answer', 'point.*')
                        ->where('answer.id', $answer)
                        ->get();
            foreach($data as $d) {
                $survey = new survey([
                    'data_peserta_id' => $request->get('id_peserta'),
                    'quest_id' => $question,
                    'answer_id' => $answer,
                    'point_id' => $d->id,
                ]);
                $survey->save();
            }

        }
        return response()->json([
            'success' => true,
            'id' => $request->get('id_peserta')
        ], 201);
    }
    public function result($id)
    {
        $percent_total_kecanduan = 0;
        $percent_total_tidak_kecanduan= 100;
        $data_peserta = data_peserta::where('id', $id)->get();
        $data = DB::table('survey')
                        ->join('data_peserta', 'survey.data_peserta_id', '=', 'data_peserta.id')
                        ->join('quest', 'survey.quest_id', '=', 'quest.id')
                        ->join('answer', 'survey.answer_id', '=', 'answer.id')
                        ->join('point', 'survey.point_id', '=', 'point.id')
                        ->select('data_peserta.*', 'point.point_percent')
                        ->where('survey.data_peserta_id', $id)
                        ->get();
        foreach($data as $d) {
            $percent_total_kecanduan = $percent_total_kecanduan + $d->point_percent;
        }
        $percent_total_tidak_kecanduan = $percent_total_tidak_kecanduan - $percent_total_kecanduan;
        return view('result', ['data' => $data, 'data_peserta' => $data_peserta, 'percent_total_kecanduan' => $percent_total_kecanduan, 'percent_total_tidak_kecanduan' => $percent_total_tidak_kecanduan]);
    }
}
