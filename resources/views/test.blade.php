<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://1.bp.blogspot.com/-USNCQDDH2uM/X61L3WayPCI/AAAAAAAAKNo/VdpAu66cUyccNPLQZC5wERji59PHOzVxQCLcBGAsYHQ/s0/UNP.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="container-fluid">
        <div class="container">
            <img src="https://1.bp.blogspot.com/-USNCQDDH2uM/X61L3WayPCI/AAAAAAAAKNo/VdpAu66cUyccNPLQZC5wERji59PHOzVxQCLcBGAsYHQ/s0/UNP.png" class="img-fluid d-block mx-auto mt-5 pt-5 mb-5" width="200">
            <div id="Judul" class="mb-5">
                <p class="text-dark text-center fs-2 fw-bold mb-1">
                    Tes Deteksi Kecanduan Bermain Game
                </p>
                <p class="text-dark text-center fs-2 fw-bold">
                    Universitas Nusantara PGRI Kediri
                </p>
            </div>
            <form id="FormQuestion">
                <div id="data_user" class="col-xl-6 col-12 d-block mx-auto">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-start fs-5 fw-bold"># Personal Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $d)
                            <input type="hidden" name="id_peserta" id="id_peserta" value="{{ $d->id }}">
                                <tr>
                                    <th scope="col" style="width: 20%">Email</th>
                                    <td>{{ $d->email }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Nama</th>
                                    <td>{{ $d->nama }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Umur</th>
                                    <td>{{ $d->umur }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Jenis Kelamin</th>
                                    <td>{{ $d->jenis_kelamin }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">TimeStamp</th>
                                    <td>{{ $d->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="quest" class="col-xl-6 col-12 d-block mx-auto my-5">
                    <p class="fs-3 fw-bold text-dark text-center">
                        Question
                    </p>
                    <div class="tab-content" id="myTabContent">
                        <input type="hidden" value="{{ $no = 1 }}">
                        <input type="hidden" name="total_question" id="total_question" value="{{ $data_count }}">
                        @foreach ($data_qa as $qa)
                            @if($no == 1)
                                <div class="tab-pane fade show active" id="tab{{ $no }}" role="tabpanel" aria-labelledby="{{ $no }}-tab">
                                    <div class="card my-3">
                                        <div class="card-header">
                                            <p class="text-dark fw-bold fs-6 mb-0">
                                            {{ $no }}. {{ $qa->question  }}
                                            </p>
                                        </div>
                                        <div class="card-body">
                                            @foreach ($qa->answer as $a)
                                                <input type="hidden" name="question_{{ $qa->id }}" id="question_{{ $qa->id }}" value="{{ $qa->id }}">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="answer_{{ $qa->id }}" id="answer_{{ $qa->id }}" value="{{ $a->id }}">
                                                    <label class="form-check-label" for="answer_{{ $qa->id }}">
                                                        {{ $a->answer }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="tab-pane fade" id="tab{{ $no }}" role="tabpanel" aria-labelledby="{{ $no }}-tab">
                                    <div class="card my-3">
                                        <div class="card-header">
                                            <p class="text-dark fw-bold fs-6 mb-0">
                                            {{ $no }}. {{ $qa->question  }}
                                            </p>
                                        </div>
                                        <div class="card-body">
                                            @foreach ($qa->answer as $a)
                                                <input type="hidden" name="question_{{ $qa->id }}" id="question_{{ $qa->id }}" value="{{ $qa->id }}">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="answer_{{ $qa->id }}" id="answer_{{ $qa->id }}" value="{{ $a->id }}">
                                                    <label class="form-check-label" for="answer_{{ $qa->id }}">
                                                        {{ $a->answer }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <input type="hidden" value="{{ $no++ }}">
                        @endforeach
                    </div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        @for ($no2 = 1; $no2 < $no; $no2++)
                            @if($no2 == 1)
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="1-tab" data-bs-toggle="tab" href="#tab{{ $no2 }}" role="tab" aria-controls="1" aria-selected="true">{{ $no2 }}</a>
                                </li>
                            @else
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="1-tab" data-bs-toggle="tab" href="#tab{{ $no2 }}" role="tab" aria-controls="1" aria-selected="true">{{ $no2 }}</a>
                                </li>
                            @endif
                        @endfor
                    </ul>
                    <button type="button" class="btn btn-danger w-100 my-4 text-white" id="submit_quest">Submit</button>
                    <button class="btn btn-primary d-none" id="loading_butt_submit_quest" type="button" disabled>
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
// Submit Quest
var submit_quest = document.getElementById('submit_quest');
submit_quest.addEventListener('click', function (event) {
    // Action to execute once the collapsible area is expanded
    $(document).ajaxStart(function () {
        $('#submit_quest').addClass( "d-none" );
        $('#loading_butt_submit_quest').removeClass( "d-none" );
    })
    .ajaxStop(function () {
        $('#submit_quest').removeClass( "d-none" );
        $('#loading_butt_submit_quest').addClass( "d-none" );
    });

    var total_question = $('#total_question').val();
    var total_q = parseInt(total_question);

    var i;
    for (i = 1; i <= total_q; i++) {
        // var quest = document.getElementsByName('quest_'+i)[0].name
        if ($('input[name="answer_'+i+'"]:checked').length == 0) {
            swal("Failed !", "Please Fill Number "+i, "error");
            return false;
        }
    }
    var formData = new FormData(FormQuestion);
    // alert(formData);
    $.ajax({
        type:"POST",
        url:"{{ route('test_post') }}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        processData: false,
        contentType: false,
        data: formData,
        success : function(data){
            var url =   '{{ url('/result') }}';
            var id  =   data.id;
            window.location.replace(url + '/' + id);
        },
        error : function(response){

        }
    });
});
</script>
</body>
</html>
