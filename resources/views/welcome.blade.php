<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://1.bp.blogspot.com/-USNCQDDH2uM/X61L3WayPCI/AAAAAAAAKNo/VdpAu66cUyccNPLQZC5wERji59PHOzVxQCLcBGAsYHQ/s0/UNP.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="container-fluid">
        <div class="container">
            <img src="https://1.bp.blogspot.com/-USNCQDDH2uM/X61L3WayPCI/AAAAAAAAKNo/VdpAu66cUyccNPLQZC5wERji59PHOzVxQCLcBGAsYHQ/s0/UNP.png" class="img-fluid d-block mx-auto mt-5 pt-5 mb-5" width="200">
            <div id="Judul" class="mb-5">
                <p class="text-dark text-center fs-2 fw-bold mb-1">
                    Tes Deteksi Kecanduan Bermain Game
                </p>
                <p class="text-dark text-center fs-2 fw-bold">
                    Universitas Nusantara PGRI Kediri
                </p>
            </div>
            <div id="data_user" class="col-xl-6 col-12 d-block mx-auto">
                <form class="row g-1" id="FormPost">
                    <div class="col-md-12">
                        <label for="email" class="form-label fs-6 fw-bold">Email</label>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="email_span"><i class="fas fa-envelope"></i></span>
                            <input type="email" class="form-control" placeholder="Email Address" aria-label="email" id="email" aria-describedby="email_span">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="nama" class="form-label fs-6 fw-bold">Nama</label>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="nama_span"><i class="fas fa-user"></i></span>
                            <input type="text" class="form-control" placeholder="Nama Lengkap" aria-label="nama" id="nama" aria-describedby="nama_span">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="umur" class="form-label fs-6 fw-bold">Umur</label>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="umur_span"><i class="fas fa-smile"></i></span>
                            <input type="number" class="form-control" placeholder="Umur Anda" aria-label="umur" id="umur" aria-describedby="umur_span">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="jenis_kelamin" class="form-label fs-6 fw-bold">Jenis Kelamin</label>
                        <select id="jenis_kelamin" class="form-select">
                            <option value="" selected>Choose...</option>
                            <option value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    <div class="col-12 my-3">
                        <button type="button" id="submit_post" class="btn btn-danger w-100 text-white fs-5">Submit</button>
                        <button class="btn btn-primary d-none" id="loading_butt_submit_post" type="button" disabled>
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Loading...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
// Add
var submit_post = document.getElementById('submit_post');
submit_post.addEventListener('click', function (event) {
    // Action to execute once the collapsible area is expanded
    $(document).ajaxStart(function () {
        $('#submit_post').addClass( "d-none" );
        $('#loading_butt_submit_post').removeClass( "d-none" );
    })
    .ajaxStop(function () {
        $('#submit_post').removeClass( "d-none" );
        $('#loading_butt_submit_post').addClass( "d-none" );
    });

    var email = $('#email').val();
    var nama = $('#nama').val();
    var umur = $('#umur').val();
    var jenis_kelamin = $('#jenis_kelamin').val();

    if(email == '' || nama == '' || umur == '' || jenis_kelamin == '')
    {
        swal("Failed !", "Please Fill Empty Field!", "error");
    }
    else
    {
        $.ajax({
            type:"POST",
            url:"{{ route('post') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data:{"email":email, "nama":nama, "umur":umur, "jenis_kelamin":jenis_kelamin},
            success : function(data){
                var url =   '{{ url('/test') }}';
                var id  =   data.id;
                window.location.replace(url + '/' + id);
            },
            error : function(response){
            }
        });
    }
});
</script>
</body>
</html>
