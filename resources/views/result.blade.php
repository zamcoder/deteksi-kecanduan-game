<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://1.bp.blogspot.com/-USNCQDDH2uM/X61L3WayPCI/AAAAAAAAKNo/VdpAu66cUyccNPLQZC5wERji59PHOzVxQCLcBGAsYHQ/s0/UNP.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/5.0.1/echarts.min.js" integrity="sha512-vMD/IRB4/cFDdU2MrTwKXOLmIJ1ULs18mzmMIWLCNYg/nZZkCdjBX+UPrtQdkleuuf0YaqXssaKk8ZXOpHo3qg==" crossorigin="anonymous"></script>
</head>
<body>
<div id="app">
    <div class="container-fluid">
        <div class="container">
            <img src="https://1.bp.blogspot.com/-USNCQDDH2uM/X61L3WayPCI/AAAAAAAAKNo/VdpAu66cUyccNPLQZC5wERji59PHOzVxQCLcBGAsYHQ/s0/UNP.png" class="img-fluid d-block mx-auto mt-5 pt-5 mb-5" width="200">
            <div id="Judul" class="mb-5">
                <p class="text-dark text-center fs-2 fw-bold mb-1">
                    Tes Deteksi Kecanduan Bermain Game
                </p>
                <p class="text-dark text-center fs-2 fw-bold">
                    Universitas Nusantara PGRI Kediri
                </p>
            </div>
            <div id="data_user" class="col-xl-6 col-12 d-block mx-auto">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-start fs-5 fw-bold"># Personal Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_peserta as $dp)
                        <input type="hidden" name="id_peserta" id="id_peserta" value="{{ $dp->id }}">
                            <tr>
                                <th scope="col" style="width: 20%">Email</th>
                                <td>{{ $dp->email }}</td>
                            </tr>
                            <tr>
                                <th scope="col">Nama</th>
                                <td>{{ $dp->nama }}</td>
                            </tr>
                            <tr>
                                <th scope="col">Umur</th>
                                <td>{{ $dp->umur }}</td>
                            </tr>
                            <tr>
                                <th scope="col">Jenis Kelamin</th>
                                <td>{{ $dp->jenis_kelamin }}</td>
                            </tr>
                            <tr>
                                <th scope="col">TimeStamp</th>
                                <td>{{ $dp->created_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="data" class="col-xl-6 col-12 d-block mx-auto mb-5 text-center">
                <div class="card w-100">
                    <div class="card-body py-5">
                        <p class="text-primary text-center fs-2 fw-bold mb-5">
                            Hasil Deteksi Kecanduan Bermain Game
                        </p>
                        <div id="pie_basic" style="width: 100%;height:400px;">
                        </div>
                    </div>
                </div>
            </div>
            {{-- {{ $percent_total }} --}}
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
// based on prepared DOM, initialize echarts instance
var myChart = echarts.init(document.getElementById('pie_basic'));

// specify chart configuration item and data
myChart.setOption({
    legend: {},
    tooltip: {},
    series : [
        {
            name: 'Reference Page',
            type: 'pie',
            radius: '55%',
            data:[
                {value:{{ $percent_total_kecanduan }}, name:'Kecanduan'},
                {value:{{ $percent_total_tidak_kecanduan }}, name:'Tidak Kecanduan'},
            ]
        }
    ]
})
</script>
</body>
</html>
