<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Route::post('/post', 'IndexController@post')->name('post');
Route::get('/test/{id}','IndexController@test')->name('test');
Route::post('/test/post', 'IndexController@test_post')->name('test_post');
Route::get('/result/{id}','IndexController@result')->name('result');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
